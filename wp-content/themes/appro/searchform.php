<?php
add_filter('get_search_form','appro_search_form');
    if( !function_exists('appro_search_form') ){
        function appro_search_form(){            
            $data = '<form role="search" method="get" class="searchform" action="'.esc_url(home_url("/")).'">';
            $data .= '<div class="search-box">';
            $data .= '<input type="search" name="s" id="search" placeholder="'. esc_attr__("Search","appro").'" value="'.get_search_query().'">';
            $data .= '</div></form>';            
            return $data;
       }
    }