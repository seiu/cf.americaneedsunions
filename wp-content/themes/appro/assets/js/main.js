(function ($) {
    $(document).on('ready', function () {
        "use strict";
        $(".carousel-inner .item:first-child").addClass("active");
        /* Mobile menu click then remove
        ==========================*/
        $(".mainmenu-area #menu-mainmenu li a").on("click", function () {
            $(".navbar-collapse").removeClass("in");
        });
        /* Scroll to top
        ===================*/
        $.scrollUp({
            scrollText: '<i class="fa fa-angle-up"></i>',
            easingType: 'linear',
            scrollSpeed: 900,
            animation: 'fade'
        });
        $("#post_photo_gallery").owlCarousel({
            // Most important owl features
            items : 1,
            singleItem : true,
            //Basic Speeds
            slideSpeed : 500,
            paginationSpeed : 800,
            rewindSpeed : 1000,    
           // Navigation
            navigation : true,
            navigationText : ["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
            rewindNav : true,
            scrollPerPage : false,
        });
        $(".post-single").fitVids();
        $('#wp-calendar').addClass('table');
        // Select all links with hashes
        $('.mainmenu-area a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .on('click',function (event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function () {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });
    });
    /* Preloader Js
    ===================*/
    $(window).on("load", function () {
        $('.preloade').fadeOut(500);
    });
})(jQuery);