<?php
/**
 * appro Theme Customizer
 *
 * @package Appro
 * @since Appro 1.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $appro_customize Theme Customizer object.
 */
function appro_customize_register( $appro_customizer ) {
		/**
		* social_menu Logo
		*/
		$appro_customizer->add_section('social_menu', array(
			'title'		=> esc_html__( 'Social Menu', 'appro' )
		));
		$appro_customizer->add_section('footer_option', array(
			'title'		=> esc_html__( 'Footer Option', 'appro' )
		));
		/*-- Facebook URL Field --*/
		$appro_customizer->add_setting('facebook', array(
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('facebook', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'Facebook','appro' ),
			'setting'		=> 'facebook'
		));
		/*-- Twitter URL Field --*/
		$appro_customizer->add_setting('twitter', array(
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('twitter', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'Twitter', 'appro' ),
			'setting'		=> 'twitter'
		));
		/*-- Linkedin URL Field --*/
		$appro_customizer->add_setting('linkedin', array(
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('linkedin', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'Linkedin', 'appro' ),
			'setting'		=> 'linkedin'
		));
		/*-- Instagram URL Field --*/
		$appro_customizer->add_setting('instagram', array(
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('instagram', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'Instagram','appro' ),
			'setting'		=> 'instagram'
		));
		/*-- Flickr URL Field --*/
		$appro_customizer->add_setting('flickr', array(
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('flickr', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'Flickr','appro' ),
			'setting'		=> 'flickr'
		));
		/*-- Pinterest URL Field --*/
		$appro_customizer->add_setting('pinterest', array(
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('pinterest', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'Pinterest', 'appro' ),
			'setting'		=> 'pinterest'
		));     
		/*-- Dribbble URL Field --*/
		$appro_customizer->add_setting('dribbble', array(
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('dribbble', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'Dribbble','appro' ),
			'setting'		=> 'dribbble'
		)); 
		/*-- Google URL Field --*/
		$appro_customizer->add_setting('google_plus', array(
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('google_plus', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'Google Plus', 'appro' ),
			'setting'		=> 'google_plus'
		));
		/*-- YouTube URL Field --*/
		$appro_customizer->add_setting('youtube', array(
			'default'		=> '',
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('youtube', array(
			'section'		=> 'social_menu',
			'label'			=> esc_html__( 'YouTube','appro' ),
			'setting'		=> 'youtube'
		));
		/*-- CopyRight Content Field --*/
		$appro_customizer->add_setting('footer_copyright', array(
			'default'		=> esc_html__( 'All &copy; copyrights reserved Design by QuomodoTheme', 'appro' ),
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('footer_copyright', array(
			'section'	=> 'footer_option',
			'label'		=> esc_html__( 'Copyright text', 'appro' ),
			'type'		=> 'textarea',
			'setting'	=> 'footer_copyright'	
		));
        /**
        *Placeholder true or false
        **/
		$appro_customizer->add_setting('appro_preloader_switch', array(
			'default'		=> '',
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('appro_preloader_switch', array(
			'section'	=> 'title_tagline',
			'label'		=> esc_html__( 'Stop Preloader', 'appro' ),
			'type'		=> 'checkbox',
			'setting'	=> 'appro_preloader_switch',
            'priority'   => 0,
		));
        /**
        *sticky_nav_menu
        **/
		$appro_customizer->add_setting('sticky_nav_menu', array(
			'default'		=> '',
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('sticky_nav_menu', array(
			'section'	=> 'title_tagline',
			'label'		=> esc_html__( 'Sticky Mainmenu', 'appro' ),
			'type'		=> 'checkbox',
			'setting'	=> 'sticky_nav_menu',
            'priority'   => 0,
		));
        /**
        *sticky_nav_menu
        **/
		$appro_customizer->add_setting('transparent_main_menu', array(
			'default'		=> '',
			'transport'		=> 'refresh'
		));
		$appro_customizer->add_control('transparent_main_menu', array(
			'section'	=> 'title_tagline',
			'label'		=> esc_html__( 'Default Transparent Menu', 'appro' ),
			'type'		=> 'checkbox',
			'setting'	=> 'transparent_main_menu',
            'priority'   => 0,
		));
}
add_action( 'customize_register', 'appro_customize_register' );
