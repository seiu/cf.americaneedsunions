<?php
if ( ! function_exists( 'appro_posted_on' ) ) :
	function appro_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		$time_string = sprintf( wp_kses( $time_string, wp_kses_allowed_html('post')),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);
        $date_format = get_the_date('Y/m/d');
        $posted_on = '<li><i class="fa fa-clock-o"></i><a href="'.esc_url(home_url($date_format)).'">'.$time_string.'</a></li>';
        if ( 'post' === get_post_type() ) {
			$categories_list = get_the_category_list(' &#44; ');
			if ( $categories_list ) {
                $categories_list = '<li><i class="fa fa-folder-o"></i> '.$categories_list.'</li>';
			}else{
                $categories_list = '';
            }
		}
        $post_author = '<li><i class="fa fa-user-o"></i> '.get_the_author().'</li>';
        printf(wp_kses('<ul class="info">%s %s %s</ul>',wp_kses_allowed_html('post')),(isset($post_author) ? $post_author : ''),(isset($posted_on) ? $posted_on : ''),(isset($categories_list) ? $categories_list : ''));
	}
endif;
if ( ! function_exists( 'appro_entry_footer' ) ) :
	function appro_entry_footer() {
        if ( 'post' === get_post_type() ) {
			$tags_list = get_the_tag_list( '', ' &#44; ' );
			if ( $tags_list ) {
				$tags_list = '<li><i class="fa fa-tags"></i> '.$tags_list.'</li>';
			}else{
                $tags_list = '';
            }
		}        
        if(current_user_can('edit_posts')){
            $edit_post = get_edit_post_link();
            $edit_post = '<li><i class="fa fa-edit"></i><a href="'.$edit_post.'">'.esc_html__('Edit Link','appro').'</a></li>';
        }        
        
        $post_author = '<li><i class="fa fa-user-o"></i> '.get_the_author().'</li>';
        printf(wp_kses('<ul class="info">%s %s %s</ul>',wp_kses_allowed_html('post')),(isset( $post_author ) ? $post_author : '' ),(isset( $tags_list ) ? $tags_list : '' ),(isset( $edit_post ) ? $edit_post : '' ));
	}
endif;
if ( ! function_exists( 'appro_post_thumbnail' ) ) :
    function appro_post_thumbnail() {
        if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
            return;
        }
        if ( is_singular() ) {
           printf(wp_kses('<div class="post-media"><figure>%s</figure></div>',wp_kses_allowed_html('post')),get_the_post_thumbnail()); 
        }else{
            printf(wp_kses('<a class="post-media" href="%s" aria-hidden="true"><figure>%s</figure></a>',wp_kses_allowed_html('post')), get_the_permalink(), get_the_post_thumbnail() );
        }
    }
endif;
if( !function_exists('post_foooter_content') ){
    function post_footer_content(){
        ?><div class="footer-content">
                <a href="<?php the_permalink(); ?>" class="read-more">
                    <?php esc_html_e('Read More','appro'); ?>
                </a>
                <?php 
                    if (! post_password_required() && ( comments_open() || get_comments_number() ) && get_comments_number() > 0) { 
                        $comment_count = get_comments_number_text(esc_html__('No comment','appro'),esc_html__('1 Comment','appro'),esc_html__('% Comments','appro'));
                        $comment_count = '<span><i class="fa fa-comments-o"></i> '.esc_html($comment_count).'</span>'; 
                        printf( $comment_count , wp_kses_allowed_html('post') );
                    }
                ?>
            </div>
        <?php
    }
}