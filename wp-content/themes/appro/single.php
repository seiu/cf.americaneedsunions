<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Appro
 * @since Appro 1.0
 */

get_header(); ?>
    <div class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="header-title">
                        <?php single_post_title(); ?>
                    </h2>
                    <div class="bread">
                        <?php
                            if( function_exists('bcn_display') ){
                                bcn_display(); 
                            }else{ 
                                bloginfo('description'); 
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 <?php echo ( is_active_sidebar('appro_main_sidebar') ? 'col-sm-9 col-md-8' : '' ); ?>">
                    <div class="blog-posts">
                        <?php
                        if(have_posts()):
                            while(have_posts()): the_post();
                                get_template_part('template_parts/content', get_post_format()); 
                            ?>
                            <div class="single-post-pagination">
                                <?php
                                        the_post_navigation(array(
                                            'prev_text' => '<i class="fa fa-angle-double-left"></i> '.esc_html__('Prev Post','appro'),
                                            'next_text' => esc_html__('Next Post','appro') . ' <i class="fa fa-angle-double-right"></i>',
                                            'screen_reader_text' => ' ',
                                            'type' => 'list'
                                        ));
                                    ?>
                            </div>
                            <?php
                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;

                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
    <?php get_footer();