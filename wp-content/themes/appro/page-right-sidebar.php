<?php
/*
 Template Name: Right Sidebar
 ****************************
 * @package Appro
 * @since Appro 1.0
*/

$tr_data = $page_header = $onepage = '';
$tr_data = get_post_meta( get_the_ID(), '_appro_transparent_menu', true );
$page_header = get_post_meta( get_the_ID(), '_appro_page_header', true );
$onepage = get_post_meta( get_the_ID(), '_appro_one_page_template', true );

if( $tr_data == 'on' ){
    $tr_data = 'v2';
}else {
    $tr_data = '';
}

if( $onepage != 'on' ){
    $s_padding = 'section-padding';
    $container = 'container';
}else {
    $s_padding = 'onepage';
    $container = 'container-fluid';
}


get_header($tr_data); ?>
  <?php if($page_header != 'on' ): ?>
    <div class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="header-title"><?php the_title(); ?></h2>
                    <div class="bread">
                        <?php if( function_exists('bcn_display') ){ bcn_display(); }else{ bloginfo('description'); } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <div class="<?php echo esc_attr($s_padding); ?>">
        <div class="<?php echo esc_attr($container); ?>">
            <div class="row">
                <div class="col-xs-12 col-md-8 <?php echo ( is_active_sidebar('appro_main_sidebar') ? 'col-sm-9 col-md-8': '' ); ?>">
                    <?php
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template_parts/content', 'page' );
                            /*If comments are open or we have at least one comment, load up the comment template.*/
                            if ( comments_open() || get_comments_number() and $onepage != 'on' ) :
                                comments_template();
                            endif;
                        endwhile;
                    ?>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php get_footer();