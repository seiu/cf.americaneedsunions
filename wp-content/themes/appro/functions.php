<?php
/**
 * @package Appro
 * @since Appro 1.0
 */

/*-- Template Directory Define --*/
define( 'APPRO_ROOT' , get_template_directory() );
/*-- Template Directory URI Define --*/
define( 'APPRO_ROOT_URI' , get_template_directory_uri() );
/*-- Image Folder Directory Define --*/
define( 'APPRO_IMAGE' , APPRO_ROOT_URI . '/assets/images' );

if( ! function_exists('appro_setup_theme') ){
    function appro_setup_theme(){
        /*
        * Make theme available for translation.
        * Translations can be filed in the /languages/ directory.
        * If you're building a theme based on Appro, use a find and replace
        * to change 'appro' to the name of your theme in all the template files.
        */
        load_theme_textdomain( 'appro', get_template_directory() . '/languages/' );
        /*Add default posts and comments RSS feed links to head.*/
        add_theme_support( 'automatic-feed-links' );

        /*
        * Let WordPress manage the document title.
        * By adding theme support, we declare that this theme does not use a
        * hard-coded <title> tag in the document head, and expect WordPress to
        * provide it for us.
        */
        add_theme_support( 'title-tag' );
        /*
        * Enable support for Post Thumbnails on posts and pages.
        *
        * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
        */
        add_theme_support( 'post-thumbnails' );
        
        if( function_exists('appro_apus_get_load_plugins') ){
            appro_apus_get_load_plugins();
        }
        /*This theme uses wp_nav_menu() in one location.*/
        register_nav_menus( array(
            'mainmenu' => esc_html__( 'Main Menu', 'appro' ),
        ) );
        /*
        * Switch default core markup for search form, comment form, and comments
        * to output valid HTML5.
        */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );
        $post_format = array(
            'aside',
            'gallery',
            'link',
            'image',
            'quote',
            'status',
            'video',
            'audio',
            'chat'
        ); 
        add_theme_support('post-formats',$post_format);        
		/*Set up the WordPress core custom background feature.*/
		add_theme_support( 'custom-background', apply_filters( 'appro_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
        add_theme_support( 'custom-header', apply_filters( 'appro_custom_header_args', array(
            'default-image'          => APPRO_IMAGE . '/site-header.jpg',
            'default-text-color'     => 'ffffff',
            'wp-head-callback'       => 'appro_header_style',
        ) ) );
        if ( ! function_exists( 'appro_header_style' ) ) {
           function appro_header_style() {
                $header_text_color = get_header_textcolor();
                if ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color ) {
                    return;
                }
            } 
        }
		/*Add theme support for selective refresh for widgets.*/
		add_theme_support( 'customize-selective-refresh-widgets' );
        
        /*This theme styles the visual editor to resemble the theme style*/
		add_editor_style( '/assets/css/custom-editor-style.css' );
        
		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'flex-width'  => true,
			'flex-height' => true,
		) );
        
        /**
         * Detect Homepage
         *
         * @return boolean value
         */
        if( !function_exists('appro_detect_homepage') ){
            function appro_detect_homepage() {
                $onepage = '';
                $onepage = get_post_meta( get_the_ID(), '_appro_one_page_template', true );
                /*If front page is set to display a static page, get the URL of the posts page.*/
                $homepage_id = get_option( 'page_on_front' );
                /*current page id*/
                $current_page_id = ( is_page( get_the_ID() ) ) ? get_the_ID() : '';
                if( $homepage_id == $current_page_id or $onepage == 'on'  ) {
                    return true;
                } else {
                    return false;
                }

            }
        }
        
    }
}

if( ! function_exists('appro_widgets_init') ){
    function appro_widgets_init(){
        $args = array(
            'name' => esc_html__('Main Sidebar','appro'),
            'id' => 'appro_main_sidebar',
            'before_title' => '<h5 class="widget-title">',
            'after_title' => '</h5>',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
        );
        register_sidebar($args);
    }
}
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function appro_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'appro_content_width', 750 );
}
/*-- Google-Fonts-Function --*/
if( !function_exists('appro_get_google_font_url') ){
    function appro_get_google_font_url(){
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext'; 
        /* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
        if ( 'off' !== esc_html_x( 'on', 'Poppins font: on or off', 'appro' ) ) {
            $fonts[] = esc_html('Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');
        }
        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), 'https://fonts.googleapis.com/css' );
        }
        return esc_url_raw( $fonts_url );
    }
}
if( ! function_exists('appro_enqueue_scripts')){
    function appro_enqueue_scripts(){
        /*-- Google-Fonts --*/
        wp_enqueue_style( 'appro-google-font', appro_get_google_font_url() );
        /*-- Stylesheets --*/
        wp_enqueue_style( 'bootstrap' , APPRO_ROOT_URI . '/assets/css/bootstrap.min.css',array(),'3.3.7', 'all' );
        wp_enqueue_style( 'owl-theme' , APPRO_ROOT_URI . '/assets/css/owl.theme.css',array(),'1.3.3', 'all' );
        wp_enqueue_style( 'owl-carousel' , APPRO_ROOT_URI . '/assets/css/owl.carousel.min.css',array(),'1.3.3', 'all' );
        wp_enqueue_style( 'themify-icon' , APPRO_ROOT_URI . '/assets/css/themify-icons.css', array(),'0.1.2','all' );
        wp_enqueue_style( 'font-awesome' , APPRO_ROOT_URI . '/assets/css/font-awesome.min.css', array(), '4.7.0', 'all' );
        wp_enqueue_style( 'appro_master_style' , APPRO_ROOT_URI . '/assets/css/master.css', array(), '1.0.0', 'all' );
        wp_enqueue_style('appro_stylesheet', get_stylesheet_uri());
        wp_enqueue_style( 'appro_responsive' , APPRO_ROOT_URI . '/assets/css/responsive.css',array(),'1.0.0','all' );
        
        /*-- Conditional-Scripts --*/
        wp_enqueue_script( 'html5shiv' , APPRO_ROOT_URI . '/assets/js/vendor/html5shiv.min.js',array(),'3.7.2', false);
        wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
        wp_enqueue_script( 'respond-js' , APPRO_ROOT_URI . '/assets/js/vendor/respond.min.js',array(),'1.4.2',false);
        wp_script_add_data( 'respond-js', 'conditional', 'lt IE 9' );            
        /*-- Plugin-Scripts --*/
        wp_enqueue_script( 'bootstrap' , APPRO_ROOT_URI . '/assets/js/vendor/bootstrap.min.js' , array('jquery'), '3.3.7', true );
        wp_enqueue_script( 'scrollUp' , APPRO_ROOT_URI . '/assets/js/scrollUp.min.js', array('jquery'), '2.4.1', true );
        /*-- There has owl-carousel two version fully different --*/
        wp_enqueue_script( 'owl-carousel' , APPRO_ROOT_URI . '/assets/js/owl.carousel.min.js', array('jquery'), '1.3.3', true );
        wp_enqueue_script( 'jquery-fitvids' , APPRO_ROOT_URI . '/assets/js/jquery.fitvids.js', array('jquery'), '1.1.0', true );
        wp_enqueue_script( 'appro_active_jquery' , APPRO_ROOT_URI . '/assets/js/main.js', array('jquery'), '1.0.0', true );
        /*-- Appro Inline Styling In The Site Header --*/
        $appro_site_header_style = 
            '.site-header {
                background-image: url('.esc_url(get_header_image()).');
                color: #'.esc_attr(get_header_textcolor()).';
            }
            .site-header .bread,
            .site-header a,
            .site-header .header-title {
                color: #'.esc_attr(get_header_textcolor()).';
            }';
        wp_add_inline_style( 'appro_master_style', $appro_site_header_style); 
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
        
    }
    
}
if(file_exists( APPRO_ROOT . '/includes/appro-tags.php' ) ){
    require( APPRO_ROOT . '/includes/appro-tags.php');
}
if(file_exists( APPRO_ROOT . '/includes/appro-functions.php' ) ){
    require( APPRO_ROOT . '/includes/appro-functions.php');
}
if(file_exists( APPRO_ROOT . '/includes/class-tgm-plugin-activation.php' ) ){
    require( APPRO_ROOT . '/includes/class-tgm-plugin-activation.php');
}
if(file_exists( APPRO_ROOT . '/includes/appro-plugin-activation.php' ) ){
    require( APPRO_ROOT . '/includes/appro-plugin-activation.php');
}
if(file_exists( APPRO_ROOT . '/includes/customizer.php' ) ){
    require( APPRO_ROOT . '/includes/customizer.php');
}
if(file_exists( APPRO_ROOT . '/includes/nav-menu-walker.php' ) ){
    require( APPRO_ROOT . '/includes/nav-menu-walker.php');
}
/*-- Action & Filter Hooks --*/
add_action( 'wp_enqueue_scripts', 'appro_enqueue_scripts' );
add_action( 'after_setup_theme', 'appro_setup_theme' );
add_action( 'widgets_init', 'appro_widgets_init' );
add_action( 'after_setup_theme', 'appro_content_width', 0 );
add_action( 'appro_after_body','appro_preloader_content', 0 );