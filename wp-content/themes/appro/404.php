<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Appro
 * @since Appro 1.0
 */
get_header(); ?>
    <div class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="header-title">
                        <?php esc_html_e( "Error Page.", 'appro' ); ?>
                    </h2>
                    <div class="bread">
                        <?php if( function_exists('bcn_display') ){ bcn_display(); }else{ bloginfo('description'); } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-padding error-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center">
                        <?php $error_image_url = APPRO_IMAGE.'/404.png'; ?>
                        <img src="<?php echo esc_url($error_image_url); ?>" alt="<?php esc_attr_e('404 image','appro'); ?>">
                        <h2 class="post-title">
                            <?php esc_html_e( "The page you are looking for doesn't exist.", 'appro' ); ?>
                        </h2>
                        <?php echo wpautop(esc_html__( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'appro' )); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>
    <?php get_footer();