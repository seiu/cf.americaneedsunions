<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_site_icon(); ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> data-spy="scroll" data-target="#mainmenu" >
   <?php do_action('appro_after_body') ?>
<?php
    $tr_check = 'yes';
    if( function_exists('appro_main_menu_content') ){
        appro_main_menu_content($tr_check);
    }