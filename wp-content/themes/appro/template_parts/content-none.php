<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package Appro
 * @since Appro 1.0
 */
?><article class="post-single" >
    <header class="post-info">
        <h2 class="post-title"><?php esc_html_e( 'Nothing Found', 'appro' ); ?></h2>
    </header>
    <div class="post-body">
        <?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
			<p><?php printf( wp_kses(__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'appro' ), array( 'a' => array( 'href' => array() ))), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
		<?php elseif ( is_search() ) : ?>
			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'appro' ); ?></p>
			<?php get_search_form(); ?>
		<?php else : ?>
			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'appro' ); ?></p>
			<?php get_search_form(); ?>
		<?php endif; ?>
    </div>
</article>