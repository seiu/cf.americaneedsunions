    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div class="social-menu">
                        <?php
                            if( function_exists('appro_social_menu_link') ){
                               appro_social_menu_link();
                            }
                        ?>
                    </div>
                    <?php 
                        $copyright = get_theme_mod( 'footer_copyright' );
                        if( $copyright ):
                    ?>
                    <div class="copyright">
                       <?php 
                        $allowed_html_tag = array(  
                            //formatting
                            'strong' => array(),
                            'em'     => array(),
                            'b'      => array(),
                            'i'      => array(),
                            'a' => array( // on allow a tags  
                                'href' => array(), // and those anchors can only have href attribute  
                                'class' => array() // and those anchors can only have class attribute  
                            )
                        ); 
                        echo wp_kses( $copyright , $allowed_html_tag );                            
                        ?>
                    </div>
                    <?php else: ?>                    
                        <div class="copyright">
                            <?php esc_html_e('All &copy; copyrights reserved ThemeCTG Design by QuomodoTheme','appro'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>