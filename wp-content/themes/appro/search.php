<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Appro
 * @since Appro 1.0
 */
get_header(); ?>
    <div class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="header-title"><?php printf( wp_kses(esc_html__( 'Search Results for: %s', 'appro' ), array( 'span' => array())), '<span>' . get_search_query() . '</span>' ); ?></h2>
                    <div class="bread">
                        <?php 
                            if( function_exists('bcn_display') ){
                                bcn_display();
                            }else{
                                bloginfo('description');
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 <?php echo ( is_active_sidebar('appro_main_sidebar') ? 'col-sm-9 col-md-8': '' ); ?>">
                    <div class="blog-posts">
                       <?php
                        if(have_posts()):
                            while(have_posts()): the_post();
                                get_template_part('template_parts/content', get_post_format());
                            endwhile;
                        else:
                            get_template_part( 'template_parts/content', 'none' );
                        endif;
                        ?>
                        <?php
                            the_posts_pagination(array(
                                'mid_size' => '6',
                                'prev_text' => 'Prev',
                                'next_text' => 'Next',
                                'screen_reader_text' => ' ',
                            ));
                        ?>
                    </div>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php get_footer();